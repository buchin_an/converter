package com.example.home.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.binar)
    TextView binar;
    @BindView(R.id.octal)
    TextView octal;
    @BindView(R.id.hex)
    TextView hex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


    }

    @OnTextChanged(R.id.value_first)
    public void setBinar(Editable editable) {
        binar.setText(Integer.toBinaryString(Integer.valueOf(String.valueOf(editable))));
    }

    @OnTextChanged(R.id.value_second)
    public void setOctal(Editable editable) {
        octal.setText(Integer.toOctalString(Integer.valueOf(String.valueOf(editable))));
    }

    @OnTextChanged(R.id.value_third)
    public void setHex(Editable editable) {
        hex.setText(Integer.toHexString(Integer.valueOf(String.valueOf(editable))));
    }

}
